# Ludum Dare 47 (03/10/2020)
Game Jam [Ludum Dare](https://ldjam.com/events/ludum-dare/47)
Thème: "Stuck in a loop"

Codé ên C++ avec [SFML](https://www.sfml-dev.org/ "Simple and Fast Multimedia Library").

---
Mathieu Arquillière - Jérémy Zangla - Sinan Daroukh - Octobre 2020