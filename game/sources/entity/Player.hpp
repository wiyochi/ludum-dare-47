#pragma once

#include <chrono>
#include <SFML/Graphics.hpp>
#include <resource_manager/ResourceManager.hpp>
#include <logger/Log.hpp>

class Player : public sf::Drawable
{
    static constexpr float SPEED = 0.1;
    static constexpr short UP = 0, RIGHT = 1, LEFT = 2, DOWN = 3, STANDING = 4;
    sf::Sprite _sprite;
    short _direction = STANDING;
    short _animation = 0;

public:
    Player();
    void draw(sf::RenderTarget&, sf::RenderStates) const override;
    void update(std::chrono::milliseconds deltaTime);
    auto getPosition() const {return _sprite.getPosition();}
};