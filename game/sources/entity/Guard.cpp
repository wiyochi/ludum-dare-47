#include <entity/Guard.hpp>

Guard::Guard() :
	m_goalIndex(0)
{
	setPosition(-1000, -1000);
}

Guard::~Guard()
{
}

void Guard::addPoint(sf::Vector2f const& goal)
{
	if (getPosition() == sf::Vector2f(-1000, -1000))
	{
		setPosition(goal);
	}
	else
	{
		m_goalPositions.push_back(goal);
		m_objective = m_goalPositions[m_goalIndex];

		// Update vision rotation
		if (m_goalPositions[m_goalIndex].x > 0)
		{
			m_direction = RIGHT;
			m_vision.setRotation(0);
		}
		else if (m_goalPositions[m_goalIndex].x < 0)
		{
			m_direction = LEFT;
			m_vision.setRotation(180);
		}
		if (m_goalPositions[m_goalIndex].y > 0)
		{
			m_direction = DOWN;
			m_vision.setRotation(90);
		}
		else if (m_goalPositions[m_goalIndex].y < 0)
		{
			m_direction = UP;
			m_vision.setRotation(270);
		}

		float angle = std::atan(m_goalPositions[m_goalIndex].y / m_goalPositions[m_goalIndex].x);
		m_movement.x = std::abs(std::cos(angle) * SPEED) * ((m_objective.x < 0) ? -1.f : 1.f);
		m_movement.y = std::abs(std::sin(angle) * SPEED) * ((m_objective.y < 0) ? -1.f : 1.f);
		chooseTexture();
	}
}

void Guard::update(std::chrono::milliseconds deltaTime)
{
	// Update position
	if (std::abs(m_objective.x) >= EPS)
	{
		move(m_movement.x * deltaTime.count() / 1000, 0);
		m_objective.x -= m_movement.x * deltaTime.count() / 1000;
	}
	if (std::abs(m_objective.y) >= EPS)
	{
		move(0, m_movement.y * deltaTime.count() / 1000);
		m_objective.y -= m_movement.y * deltaTime.count() / 1000;
	}

	// Reach the goal
	if (std::abs(m_objective.y) < EPS && std::abs(m_objective.x) < EPS)
	{
		// Round position to the goal
		move(m_objective);
		setPosition(sf::Vector2f(std::round(getPosition().x), std::round(getPosition().y)));

		// Changing goal
		m_goalIndex = (m_goalIndex + 1) % m_goalPositions.size();
		m_objective = m_goalPositions[m_goalIndex];

		// Update vision rotation
		if (m_goalPositions[m_goalIndex].x > 0)
		{
			m_direction = RIGHT;
			m_vision.setRotation(0);
		}
		else if (m_goalPositions[m_goalIndex].x < 0)
		{
			m_direction = LEFT;
			m_vision.setRotation(180);
		}
		if (m_goalPositions[m_goalIndex].y > 0)
		{
			m_direction = DOWN;
			m_vision.setRotation(90);
		}
		else if (m_goalPositions[m_goalIndex].y < 0)
		{
			m_direction = UP;
			m_vision.setRotation(270);
		}

		float angle = std::atan(m_goalPositions[m_goalIndex].y / m_goalPositions[m_goalIndex].x);
		m_movement.x = std::abs(std::cos(angle) * SPEED) * ((m_objective.x < 0) ? -1.f : 1.f);
		m_movement.y = std::abs(std::sin(angle) * SPEED) * ((m_objective.y < 0) ? -1.f : 1.f);
	}

	chooseTexture();
}

void Guard::chooseTexture()
{
	int texture = 0;

	switch (m_direction)
	{
	case UP:
		++m_animation %= 100;
		texture = 14 + m_animation / 50;
		break;
	case DOWN:
		++m_animation %= 100;
		texture = 2 + m_animation / 50;
		break;
	case RIGHT:
		++m_animation %= 200;
		texture = 6 + m_animation / 50;
		break;
	case LEFT:
		++m_animation %= 200;
		texture = 10 + m_animation / 50;
		break;
	case STANDING:
		texture = 1;
		break;
	}

	m_sprite.setTexture(ResourceManager::getInstance().getTexture("guards" + std::to_string(texture)));
}

void Guard::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_vision, states);
	target.draw(m_sprite, states);
}