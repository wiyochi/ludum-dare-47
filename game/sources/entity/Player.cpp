#include "Player.hpp"

Player::Player()
{
    _sprite.setTexture(ResourceManager::getInstance().getTexture("player1"));
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(_sprite, states);
}

void Player::update(std::chrono::milliseconds deltaTime)
{
    int texture = 0;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
	{
		_direction = UP;
		_sprite.move(0, -SPEED * deltaTime.count());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		_direction = DOWN;
		_sprite.move(0, SPEED * deltaTime.count());
	}
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
        _direction = LEFT;
        _sprite.move(-SPEED * deltaTime.count(), 0);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		_direction = RIGHT;
		_sprite.move(SPEED * deltaTime.count(), 0);
	}

    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Z)
        && !sf::Keyboard::isKeyPressed(sf::Keyboard::Q)
        && !sf::Keyboard::isKeyPressed(sf::Keyboard::S)
        && !sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        _direction = STANDING;
    }
    
    switch (_direction)
    {
        case UP:
            ++_animation %= 100;
            texture = 12 + _animation / 50;
            break;
        case DOWN:
            ++_animation %= 100;
            texture = 2 + _animation / 50;
            break;
        case RIGHT:
            ++_animation %= 200;
            texture = 4 + _animation / 50;
            break;
        case LEFT:
            ++_animation %= 200;
            texture = 8 + _animation / 50;
            break;
        case STANDING:
            texture = 1;
            break;
    }

    _sprite.setTexture(ResourceManager::getInstance().getTexture("player" + std::to_string(texture)));
}