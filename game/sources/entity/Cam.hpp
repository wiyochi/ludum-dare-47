#pragma once

#include <chrono>
#include <SFML/Graphics.hpp>
#include <entity/Triangle.hpp>
#include <logger/Log.hpp>

class Cam : public sf::Drawable
{
public:
	Cam();
	~Cam();

	void update(std::chrono::milliseconds deltaTime);

	void setPosition(sf::Vector2f const& pos);
	void setRotation(float angle);

	bool detect(sf::Vector2f const& point) { return m_vision.contains(point); }

private:
	sf::CircleShape m_shape;

	Triangle m_vision;
	float m_angle;

	std::chrono::milliseconds m_timeCounter;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};