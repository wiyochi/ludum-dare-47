#pragma once

#include <chrono>
#include <SFML/Graphics.hpp>
#include <logger/Log.hpp>
#include <cmath>

class Triangle : public sf::Drawable
{
public:
	Triangle();
	~Triangle();

	sf::Vector2f const& getPosition() const { return m_shape[0].position; }
	void setPosition(sf::Vector2f const& point);
	void move(sf::Vector2f const& offset);
	void setRotation(float angle);

	bool contains(sf::Vector2f const& point);

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	float m_rotation;
	sf::VertexArray m_shape;
};