#pragma once

#include <chrono>
#include <vector>
#include <SFML/Graphics.hpp>
#include <logger/Log.hpp>
#include <entity/Triangle.hpp>
#include <cmath>
#include <resource_manager/ResourceManager.hpp>

class Guard : public sf::Drawable
{
public:
	Guard();
	~Guard();

	void update(std::chrono::milliseconds deltaTime);
	void chooseTexture();

	void move(float dX, float dY)
	{
		m_sprite.move(dX, dY);
		m_vision.move(sf::Vector2f(dX, dY));
	}

	void move(sf::Vector2f const& dPos)
	{
		move(dPos.x, dPos.y);
	}

	void setPosition(float dX, float dY)
	{
		m_sprite.setPosition(dX, dY);
		m_vision.setPosition(sf::Vector2f(dX + 10, dY + 10));
	}

	void setPosition(sf::Vector2f const& newPos)
	{
		setPosition(newPos.x, newPos.y);
	}

	sf::Vector2f const & getPosition() const
	{
		return m_sprite.getPosition();
	}

	void addPoint(sf::Vector2f const & goal);

	bool detect(sf::Vector2f const& point) { return m_vision.contains(point); }

private:
	static constexpr float EPS = 5.f;
	static constexpr float SPEED = 50.f;

	static constexpr short UP = 0, RIGHT = 1, LEFT = 2, DOWN = 3, STANDING = 4;
	sf::Sprite m_sprite;
	short m_direction = STANDING;
	short m_animation;

	std::vector<sf::Vector2f> m_goalPositions;
	int m_goalIndex;

	sf::Vector2f m_objective;
	sf::Vector2f m_movement;

	Triangle m_vision;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

