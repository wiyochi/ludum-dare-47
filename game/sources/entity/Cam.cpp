#include <entity/Cam.hpp>

Cam::Cam() :
	m_angle(0),
	m_timeCounter(0)
{
	m_shape.setFillColor(sf::Color::Red);
	m_shape.setRadius(5);
	m_shape.setOrigin(sf::Vector2f(5, 5));
}

Cam::~Cam()
{
}

void Cam::update(std::chrono::milliseconds deltaTime)
{
	setRotation(m_angle);
	m_angle += deltaTime.count() * 0.1;
	if (m_angle > 360)
		m_angle = 0;
}

void Cam::setPosition(sf::Vector2f const& pos)
{
	m_shape.setPosition(pos);
	m_vision.setPosition(pos);
}

void Cam::setRotation(float angle)
{
	m_shape.setRotation(angle);
	m_vision.setRotation(angle);
}

void Cam::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_shape, states);
	target.draw(m_vision, states);
}
