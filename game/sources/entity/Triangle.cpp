#include <entity/Triangle.hpp>


Triangle::Triangle() :
	m_rotation(0),
	m_shape(sf::Triangles, 3)
{
	m_shape[0].color = sf::Color(255, 255, 0, 255);
	m_shape[1].color = sf::Color(160, 160, 0, 150);
	m_shape[2].color = sf::Color(160, 160, 0, 150);
}

Triangle::~Triangle()
{
}

void Triangle::setPosition(sf::Vector2f const& point)
{
	m_shape[0].position = point;
	sf::Vector2f a = sf::Vector2f(point.x + 50 * std::cos(m_rotation / 180 * 3.1415), point.y + 50 * std::sin(m_rotation / 180 * 3.1415));
	m_shape[1].position = sf::Vector2f(a.x + 20 * std::cos((m_rotation + 90) / 180 * 3.1415), a.y + 20 * std::sin((m_rotation + 90) / 180 * 3.1415));
	m_shape[2].position = sf::Vector2f(a.x - 20 * std::cos((m_rotation + 90) / 180 * 3.1415), a.y - 20 * std::sin((m_rotation + 90) / 180 * 3.1415));
}

void Triangle::move(sf::Vector2f const& offset)
{
	setPosition(sf::Vector2f(getPosition().x + offset.x, getPosition().y + offset.y));
}

void Triangle::setRotation(float angle)
{
	m_rotation = angle;
	setPosition(m_shape[0].position);
}

bool Triangle::contains(sf::Vector2f const& point)
{
	sf::Vector2f p1 = m_shape[0].position;
	sf::Vector2f p2 = m_shape[1].position;
	sf::Vector2f p3 = m_shape[2].position;

	float areaOrig = std::abs((p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y));

	float area1 = std::abs((p1.x - point.x) * (p2.y - point.y) - (p2.x - point.x) * (p1.y - point.y));
	float area2 = std::abs((p2.x - point.x) * (p3.y - point.y) - (p3.x - point.x) * (p2.y - point.y));
	float area3 = std::abs((p3.x - point.x) * (p1.y - point.y) - (p1.x - point.x) * (p3.y - point.y));

	return (area1 + area2 + area3) == areaOrig;
}

void Triangle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_shape, states);
}
