#include "Log.hpp"


unsigned Log::global_level = Level::INFO | Level::WARN | Level::ERROR | Level::CRITICAL;

void Log::change_log_level(Level level)
{
    global_level = 0;
    for (unsigned i = level; i < level_n; i = (i << 1))
    {
        global_level |= i;
    }
}

void Log::show_level(Level level)
{
    global_level |= level;
}

void Log::hide_level(Level level)
{
    global_level ^= level;
}

std::string Log::reset_color()
{
    return "\033[0m";
}

std::string Log::getLevelString(Level level)
{
    std::string color = "[";
    switch (level)
    {
    case Level::DEBUG:
        color += "\033[0;94mDEBUG";
        break;
    case Level::INFO:
        color += "\033[0;92mINFO";
        break;
    case Level::WARN:
        color += "\033[1;93mWARN";
        break;
    case Level::ERROR:
        color += "\033[1;91mERROR";
        break;
    case Level::CRITICAL:
        color += "\033[1;101mCRIT";
        break;
    case Level::level_n:
    default:
        color += "\033[0m";
    }
    return color + reset_color() + "]";
}

void Log::print_level(Level level)
{
    std::cout << getLevelString(level) << "\t";
}

void Log::print_place(std::string const & place)
{
    std::cout << "[" << place << "] ";
}

void Log::test()
{
    Log::debug("LOG_TEST", "DEBUG");
    Log::info("LOG_TEST", "INFO");
    Log::warn("LOG_TEST", "WARNing");
    Log::error("LOG_TEST", "ERROR");
    Log::critical("LOG_TEST", "CRITICAL");
}