/**
 * @file Log.hpp
 * @author Wiyochi
 * @brief Simple logger (only a visual abstraction)
 * @version 0.1
 * @date 2020-03-17
 * 
 * @copyright Copyright (c) 2020
 * 
 * To use it :
 *  - Log::<level>("Place", "msg", ...); with level in { debug, info, warn, error, critical }
 *  - change_log_level(<LEVEL>) change the lowest level displayed with LEVEL in { DEBUG, INFO, WARN, ERROR, CRITICAL }
 *  - hide(<LEVEL>) hide all messages with that level "
 *  - show(<LEVEL>) show all messages with that level "
 * 
 */
#pragma once

#include <iostream>
#include <string>

class Log
{
public:
    enum Level : unsigned
    {
        DEBUG = 1,
        INFO = 2,
        WARN = 4,
        ERROR = 8,
        CRITICAL = 16,
        level_n
    };

    static void change_log_level(Level level = INFO);
    static void show_level(Level level);
    static void hide_level(Level level);

    template<typename... Args>
    static void log(Level level, std::string const & place, Args... args)
    {
        if (global_level & level)
        {
            print_level(level);
            print_place(place);
            print_msg(args...);
        }
    }

    template<typename... Args>
    static constexpr auto debug(std::string const & place, Args... args)
    {
        log(Log::DEBUG, place, args...);
    }
    template<typename... Args>
    static constexpr auto info(std::string const & place, Args... args)
    {
        log(Log::INFO, place, args...);
    }
    template<typename... Args>
    static constexpr auto warn(std::string const & place, Args... args)
    {
        log(Log::WARN, place, args...);
    }
    template<typename... Args>
    static constexpr auto error(std::string const & place, Args... args)
    {
        log(Log::ERROR, place, args...);
    }
    template<typename... Args>
    static constexpr auto critical(std::string const & place, Args... args)
    {
        log(Log::CRITICAL, place, args...);
    }

    static void test();

private:
    static std::string reset_color();
    static std::string getLevelString(Level level);
    static void print_level(Level level);
    static void print_place(std::string const & place);

    static unsigned global_level;

    template<typename T>
    static void print_msg(T arg)
    {
        std::cout << arg << std::endl;
    }

    template<typename T, typename... Args>
    static void print_msg(T arg, Args... args)
    {
        std::cout << arg;
        print_msg(args...);
    }
};