#include "ResourceManager.hpp"

void ResourceManager::loadAllResources()
{
    load(TEXTURE_DIR, _textures);
    load(FONT_DIR, _fonts);
    load(SOUND_DIR, _sounds);
}

sf::Texture const & ResourceManager::getTexture(std::string const & texture_name)
{
    return *_textures.at(texture_name);
}

sf::Font const & ResourceManager::getFont(std::string const & font_name)
{
    return *_fonts.at(font_name);
}

sf::SoundBuffer const & ResourceManager::getSound(std::string const & sound_name)
{
    return *_sounds.at(sound_name);
}