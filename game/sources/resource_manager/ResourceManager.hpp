#pragma once

#include <map>
#include <string>
#include <filesystem>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <resource_manager/Singleton.hpp>
#include <logger/Log.hpp>

namespace fs = std::filesystem;

class ResourceManager : public Singleton<ResourceManager>
{
private:
    static constexpr const char * TEXTURE_DIR = "resources/textures/";
    static constexpr const char * FONT_DIR = "resources/fonts/";
    static constexpr const char * SOUND_DIR = "resources/sounds/";

    std::map<std::string, std::shared_ptr<sf::Texture>>     _textures;
    std::map<std::string, std::shared_ptr<sf::Font>>        _fonts;
    std::map<std::string, std::shared_ptr<sf::SoundBuffer>> _sounds;

    template<typename T>
    static void load(const char * path, std::map<std::string, std::shared_ptr<T>>& map) {
        auto fullPath = gameRootPath() + path;
        for (auto &&file : fs::recursive_directory_iterator(fullPath))
        {
            if (file.is_regular_file())
            {
                auto resource = std::make_shared<T>();
                if (resource->loadFromFile(file.path().string()))
                {
                    map[file.path().filename().replace_extension("").string()] = resource;
                    Log::info("RESOURCE_MANAGER", file.path(), " loaded");
                }
                else
                {
                    Log::warn("RESOURCE_MANAGER", file.path(), " couldn't be loaded");
                }
            }
        }
    }

public:
    void loadAllResources();

    sf::Texture const & getTexture(std::string const &);
    sf::Font const & getFont(std::string const &);
    sf::SoundBuffer const & getSound(std::string const &);

    static std::string gameRootPath()
    {
        #ifdef WIN32
        return std::filesystem::current_path().string() + "/";
        #else
        return std::filesystem::current_path().string() + "/game/";
        #endif
    }
};