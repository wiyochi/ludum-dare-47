#pragma once
#include <memory>

template<class T>
class Singleton
{
public:
    Singleton() {}
    Singleton(Singleton const &) = delete;
    Singleton& operator=(Singleton const &) = delete;

    static T& getInstance() {
        static const auto instance = std::make_unique<T>();
        return *instance;
    }
};