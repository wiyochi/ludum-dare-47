#include "Level1.hpp"

Level1::Level1() : Level("lvl1.txt"),
    _bg_key_Z(sf::RectangleShape(sf::Vector2f(50, 50))),
    _bg_key_Q(sf::RectangleShape(sf::Vector2f(50, 50))),
    _bg_key_S(sf::RectangleShape(sf::Vector2f(50, 50))),
    _bg_key_D(sf::RectangleShape(sf::Vector2f(50, 50))),
    _hud(sf::FloatRect(-960, -1010, 240, 120)),
    _letter_hud(sf::FloatRect(-2000, -2200, 720, 800))
{
    _bg_key_Z.setTexture(&ResourceManager::getInstance().getTexture("key"));
    _bg_key_Q.setTexture(&ResourceManager::getInstance().getTexture("key"));
    _bg_key_S.setTexture(&ResourceManager::getInstance().getTexture("key"));
    _bg_key_D.setTexture(&ResourceManager::getInstance().getTexture("key"));

    _bg_key_Z.setPosition(-900, -1010);
    _bg_key_Q.setPosition(-960, -950);
    _bg_key_S.setPosition(-900, -950);
    _bg_key_D.setPosition(-840, -950);

    _fg_key_Z.setFont(ResourceManager::getInstance().getFont("Roboto-Thin"));
    _fg_key_Q.setFont(ResourceManager::getInstance().getFont("Roboto-Thin"));
    _fg_key_S.setFont(ResourceManager::getInstance().getFont("Roboto-Thin"));
    _fg_key_D.setFont(ResourceManager::getInstance().getFont("Roboto-Thin"));

    _fg_key_Z.setString("Z");
    _fg_key_Q.setString("Q");
    _fg_key_S.setString("S");
    _fg_key_D.setString("D");

    _fg_key_Z.setPosition(-900 + 15, 5 -1010);
    _fg_key_Q.setPosition(-960 + 15, 5 -950);
    _fg_key_S.setPosition(-900 + 15, 5 -950);
    _fg_key_D.setPosition(-840 + 15, 5 -950);

    _hud.setViewport(sf::FloatRect(0.01f, 0.90f, 0.1f, 0.1f));
    _letter_hud.setViewport(sf::FloatRect(0.01f, 0.2f, 0.30f, 0.4f));

    _letter.setFont(ResourceManager::getInstance().getFont("Roboto Mono Medium Nerd Font Complete"));
    _letter.setString(_letterText);
    _letter.setPosition(-2000, -2000);
    
    _guardMessage.setFont(ResourceManager::getInstance().getFont("Roboto Mono Medium Nerd Font Complete"));
    _guardMessage.setString(_guardText);
    _guardMessage.setPosition(-2000, -2200);

    _guardMessage.setFillColor(sf::Color(245, 54, 5, 200));
    _letter.setFillColor(sf::Color(245, 243, 203, 150));
}


void Level1::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    Level::draw(target, states);
    const auto& backup = target.getView();
    target.setView(_hud);

    target.draw(_bg_key_Z);
    target.draw(_bg_key_Q);
    target.draw(_bg_key_S);
    target.draw(_bg_key_D);

    target.draw(_fg_key_Z);
    target.draw(_fg_key_Q);
    target.draw(_fg_key_S);
    target.draw(_fg_key_D);    

    target.setView(_letter_hud);
    target.draw(_letter, states);
    target.draw(_guardMessage, states);

    target.setView(backup);
}