#include "Level.hpp"

std::map<char, char const *> Level::_texturesTable;
std::map<char, sf::IntRect> Level::_texturesRect;

Level::Level(std::string const & inputFile):
    _overlay(sf::Vector2f(16*30, 9*30))
{
    auto path = ResourceManager::gameRootPath() + "resources/maps/";

    _overlay.setTexture(&ResourceManager::getInstance().getTexture("overlay"));
    _overlay.setOrigin(_overlay.getSize().x / 2, _overlay.getSize().y / 2);

    std::string line;
    std::ifstream ifs(path + inputFile, std::ios::in);

    while (!ifs.eof())
    {
        ifs >> line;
        if (line[0] != '#') // Chargement d'une ligne du level
        {
            _map.push_back(line);
        }
        else // Chargement d'une ligne de macro
        {
            if (line[1] == 'G') // C'est un garde
            {
                std::stringstream ss;
                char dump;
                int x, y;
                auto guard = std::make_shared<Guard>();
                ss << line;
                ss >> dump >> dump; // Suppression des caractères de définition de macro
                while(!ss.eof())
                {
                    ss >> x;
                    ss >> dump;
                    ss >> y;
                    ss >> dump;
                    if (!ss.fail())
                        guard->addPoint(sf::Vector2f(x, y));
                }

                _guards.push_back(guard);
			}
            if (line[1] == 'C') // C'est un garde
            {
				std::stringstream ss;
				char dump;
				int x, y;
				auto cam = std::make_shared<Cam>();
				ss << line;
				ss >> dump >> dump; // Suppression des caractères de définition de macro
				ss >> x;
				ss >> dump;
				ss >> y;
				ss >> dump;
				if (!ss.fail())
					cam->setPosition(sf::Vector2f(x, y));
                _cams.push_back(cam);
            }
            else // Si on ajoute des macros autres
            {
                
            } 
        }
    }

    // Création de la _renderMap.
    int x, y = 0;
    for (auto && line : _map)
    {
        x = 0;
        for (auto && c : line)
        {
            auto rect = std::make_shared<sf::RectangleShape>(sf::Vector2f(BLOCK_SIZE, BLOCK_SIZE));

            rect->setPosition(x * BLOCK_SIZE, y * BLOCK_SIZE);

            rect->setTexture(&ResourceManager::getInstance().getTexture(_texturesTable[c]));
            rect->setTextureRect(_texturesRect[c]);

            _renderMap.push_back(rect);
            ++x;
        }
        ++y;
    }
}

void Level::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    for (auto && rect : _renderMap)
    {
        target.draw(*rect, states);
    }

    for (auto && guard : _guards)
    {
        target.draw(*guard, states);
	}

	for (auto&& cam : _cams)
	{
		target.draw(*cam, states);
	}
    target.draw(_player, states);
    target.draw(_overlay, states);
}

    
void Level::update(std::chrono::milliseconds const & dt)
{
	for (auto&& guard : _guards) guard->update(dt);
	for (auto&& cam : _cams) cam->update(dt);
    _player.update(dt);
    _overlay.setPosition(getPlayerPosition());

    sf::FloatRect playerRect = sf::FloatRect(_player.getPosition().x, _player.getPosition().y, 16, 16);
    for (auto&& guard : _guards)
    {
        // Guard detects if the player is inside his light (guard.detect) and if the player is too close (guardRect)
        float guardSensibility = 0.f;
        sf::FloatRect guardRect = sf::FloatRect(guard->getPosition().x - guardSensibility, guard->getPosition().y - guardSensibility, 16 + guardSensibility, 16 + guardSensibility);

        if (guard->detect(sf::Vector2f(playerRect.left, playerRect.top))
            || guard->detect(sf::Vector2f(playerRect.left + playerRect.width, playerRect.top))
            || guard->detect(sf::Vector2f(playerRect.left, playerRect.top + playerRect.height))
            || guard->detect(sf::Vector2f(playerRect.left + playerRect.width, playerRect.top + playerRect.height))
            || guardRect.intersects(playerRect))
        {
            // Game over
            _state = LOSE;
        }
    }
    for (auto&& cam : _cams)
    {
		if (cam->detect(sf::Vector2f(playerRect.left, playerRect.top))
			|| cam->detect(sf::Vector2f(playerRect.left + playerRect.width, playerRect.top))
			|| cam->detect(sf::Vector2f(playerRect.left, playerRect.top + playerRect.height))
			|| cam->detect(sf::Vector2f(playerRect.left + playerRect.width, playerRect.top + playerRect.height)))
		{
			// Game over
			_state = LOSE;
		}
    }
}

std::string const & Level::operator[](int i) const
{
    return _map[i];
}