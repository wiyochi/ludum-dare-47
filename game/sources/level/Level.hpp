#pragma once

#include <SFML/Graphics.hpp>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <filesystem>
#include <map>
#include <memory>
#include <entity/Player.hpp>
#include "../resource_manager/ResourceManager.hpp"
#include <entity/Guard.hpp>
#include <entity/Cam.hpp>

class Level : public sf::Drawable
{
public:
    enum GameState
    {
        INGAME,
        WIN,
        LOSE
    };

private:
    // Représentation de chaque case par un caractère.
    std::vector<std::string> _map;
	std::vector<std::shared_ptr<Guard>> _guards;
	std::vector<std::shared_ptr<Cam>> _cams;
    std::vector<std::shared_ptr<sf::RectangleShape>> _renderMap;

    sf::RectangleShape _overlay;
    Player _player;

    static std::map<char, char const *> _texturesTable;
    static std::map<char, sf::IntRect> _texturesRect;

    static constexpr int BLOCK_SIZE = 16;

    GameState _state;

public:
    Level(std::string const &);
    void draw(sf::RenderTarget &, sf::RenderStates) const override;
    virtual void update(std::chrono::milliseconds const &);
    auto getPlayerPosition() {return sf::Vector2f(_player.getPosition().x + 8, _player.getPosition().y + 8);}
    GameState const& getState() const { return _state; }

    static void loadTable()
    {
        
        _texturesTable['A'] = "tileset";
        _texturesTable['B'] = "tileset";
        _texturesTable['C'] = "tileset";
        _texturesTable['D'] = "tileset";
        _texturesTable['E'] = "tileset";
        _texturesTable['F'] = "tileset";
        _texturesTable['G'] = "tileset";
        _texturesTable['H'] = "tileset";
        _texturesTable['I'] = "tileset";

        _texturesRect['A'] = sf::IntRect(00, 0, 16, 16);
        _texturesRect['B'] = sf::IntRect(16, 0, 16, 16);
        _texturesRect['C'] = sf::IntRect(32, 0, 16, 16);
        _texturesRect['D'] = sf::IntRect(00, 16, 16, 16);
        _texturesRect['E'] = sf::IntRect(16, 16, 16, 16);
        _texturesRect['F'] = sf::IntRect(32, 16, 16, 16);
        _texturesRect['G'] = sf::IntRect(00, 32, 16, 16);
        _texturesRect['H'] = sf::IntRect(16, 32, 16, 16);
        _texturesRect['I'] = sf::IntRect(32, 32, 16, 16);
    }

    //Accès à une case de la grille
    std::string const & operator[](int) const;

};