#pragma once

#include "Level.hpp"

class Level1 : public Level
{
    sf::RectangleShape _bg_key_Z;
    sf::RectangleShape _bg_key_Q;
    sf::RectangleShape _bg_key_S;
    sf::RectangleShape _bg_key_D;

    sf::Text _fg_key_Z;
    sf::Text _fg_key_Q;
    sf::Text _fg_key_S;
    sf::Text _fg_key_D;

    sf::Text _letter, _guardMessage;

    sf::View _hud, _letter_hud;

    static constexpr auto _letterText = "\"Hi Billy,\nWe miss you a lot during the past \nfive years. \nNothing really matter\nsince you get arrested except Cobra. \nHe starts to hate his job and he\ndoesn't want to be a teacher anymore. \nYou should send him a letter to motivate \nhim.\n\nI hope you're fine,\nCfy\"";

    static constexpr auto _guardText = "You received a letter !";

public:
    Level1();

    void draw(sf::RenderTarget&, sf::RenderStates) const override;
};