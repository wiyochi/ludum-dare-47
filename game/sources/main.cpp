#include <chrono>
#include <SFML/Graphics.hpp>
#include "level/Level1.hpp"
#include <resource_manager/ResourceManager.hpp>
#include <entity/Guard.hpp>
#include <entity/Triangle.hpp>
#include <entity/Cam.hpp>
#include <thread>

enum GameState {MENU, INGAME, DEAD};

int main()
{
	Log::change_log_level(Log::DEBUG);
    GameState state = MENU;
    sf::RenderWindow window(sf::VideoMode(16*100., 9*100.), "LD47");
    sf::View view = window.getView();
    view.setSize(16*30, 9*30);
    window.setFramerateLimit(120);
    int alpha = 51;
    int dt = 1;

    ResourceManager::getInstance().loadAllResources();
    Level::loadTable();

    std::unique_ptr<Level1> lvl;

    sf::RectangleShape titleScreen(sf::Vector2f(16*100, 9*100));
    titleScreen.setTexture(&ResourceManager::getInstance().getTexture("title_screen"));
    sf::Text pressEnterToPlay;
    pressEnterToPlay.setString("Press [Enter] to start a new game");
    pressEnterToPlay.setFont(ResourceManager::getInstance().getFont("Roboto Mono Medium Nerd Font Complete"));
    pressEnterToPlay.setOrigin(pressEnterToPlay.getLocalBounds().width / 2, pressEnterToPlay.getLocalBounds().height / 2);
    pressEnterToPlay.setPosition(16*50, 9*70);


	sf::Text deadText;
    deadText.setString("You have been catched !");
    deadText.setFont(ResourceManager::getInstance().getFont("Roboto Mono Medium Nerd Font Complete"));
    deadText.setOrigin(deadText.getLocalBounds().width / 2, deadText.getLocalBounds().height / 2);
	deadText.setPosition(16 * 50, 6 * 70);
	sf::Text deadText2;
	deadText2.setString("Press [Enter] to retry");
	deadText2.setFont(ResourceManager::getInstance().getFont("Roboto Mono Medium Nerd Font Complete"));
	deadText2.setOrigin(deadText2.getLocalBounds().width / 2, deadText2.getLocalBounds().height / 2);
	deadText2.setPosition(16 * 50, 7 * 70);

    std::chrono::milliseconds deadAnimTimeCounter(0);
    float dark = 0;
	sf::CircleShape circleDark(5000);
    circleDark.setFillColor(sf::Color::Black);
    circleDark.setOrigin(circleDark.getRadius(), circleDark.getRadius());
    circleDark.setPosition(16 * 50, 7 * 70);

    std::chrono::milliseconds deltaTime(0);
    std::chrono::steady_clock::time_point start, end;

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Enter && (state == GameState::MENU || state == GameState::DEAD))
            {
				state = GameState::INGAME;
				lvl = std::make_unique<Level1>();
            }

        }

        window.clear();

        switch (state)
        {
            case GameState::MENU:
                window.draw(titleScreen);
                window.draw(pressEnterToPlay);
                pressEnterToPlay.setFillColor(sf::Color(200, 200, 200, alpha));
                alpha+=dt;
                if (alpha <= 50 || alpha >= 200) dt = -dt;
                break;
            case GameState::INGAME:
                start = std::chrono::high_resolution_clock::now();

                // Update
                lvl->update(deltaTime);
                
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                end = std::chrono::high_resolution_clock::now();

                deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

                view.setCenter(lvl->getPlayerPosition());
                window.setView(view);

                window.draw(*lvl);


                if (lvl->getState() == Level::WIN)
                {
                    state = MENU;
                }
				else if (lvl->getState() == Level::LOSE)
				{
                    state = DEAD;
                    dark = 0;
                    start = std::chrono::high_resolution_clock::now();
				}
                break;
			case GameState::DEAD:
                deadAnimTimeCounter = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start);
                if (deadAnimTimeCounter < std::chrono::seconds(2))
                {
					dark = (dark + 1 <= 255) ? dark + 1 : 255;

					view.setCenter(lvl->getPlayerPosition());
					window.setView(view);

					window.draw(*lvl);

					window.setView(window.getDefaultView());
					circleDark.setFillColor(sf::Color(0, 0, 0, dark));
					window.draw(circleDark);
					window.draw(deadText);
                }
                else
				{
					window.setView(window.getDefaultView());
					window.draw(deadText);
					window.draw(deadText2);
                }

				deadText2.setFillColor(sf::Color(200, 200, 200, alpha));
				alpha += dt;
				if (alpha <= 50 || alpha >= 200) dt = -dt;
                break;
        }
		
        window.display();
    }

    return 0;
}


//#include <SFML/Graphics.hpp>
//
//int main()
//{
//	// Create a window for the game
//	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Application");
//
//	// Load the center sprite
//	sf::Texture texture;
//	texture.loadFromFile("background.png");
//
//	sf::Sprite center_sprite(texture);
//	center_sprite.setOrigin(256, 256);
//	center_sprite.setPosition(400, 300);
//
//	// Overlaping circle for mode demonstration
//	sf::CircleShape circle(150);
//	circle.setFillColor(sf::Color(120, 120, 200, 128));
//	circle.setOutlineThickness(40);
//	circle.setOutlineColor(sf::Color(200, 200, 20, 200));
//	circle.setOrigin(150, 150);
//
//	for (;;)
//	{
//		// ---------- Handle Events -----------
//		sf::Event event;
//		while (window.pollEvent(event))
//		{
//			// Handle events
//			if (event.type == sf::Event::Closed
//				|| event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
//				return 0;
//		}
//
//		// ---------- Draw the scene ----------
//		window.clear();
//
//		// Draw the center textured sprite
//		window.draw(center_sprite);
//
//		// Draw the four pre-defined blending modes
//		//circle.setPosition(144, 44);
//		//window.draw(circle, sf::BlendAlpha);
//		//circle.setPosition(656, 44);
//		//window.draw(circle, sf::BlendAdd);
//		circle.setPosition(656, 556);
//		window.draw(circle, sf::BlendMultiply);
//		//circle.setPosition(144, 556);
//		//window.draw(circle, sf::BlendNone);
//
//		// Create and draw a modulation blending mode
//		//sf::BlendMode blendmode = sf::BlendMultiply;
//		//blendmode.colorDstFactor = sf::BlendMode::SrcColor;
//		//circle.setScale(.75f, .75f);
//		//circle.setPosition(400, 300);
//		//window.draw(circle, blendmode);
//		//circle.setScale(1.f, 1.f);
//
//		// Display the frame
//		window.display();
//	}
//}